(*
   JOHN KARASEV
   HW#4
   CS355 WSUV SPRING 2018
*)

signature TREE =
sig

  type tree

  val create: int list -> tree (*take a int list return a tree*)

  val add : (int*tree) -> tree (*take a int and a tree, add the int intot the tree*)

  val lookup : (int*tree) -> bool (*take a int and tree return true if the int exits in the tree*)

  val rangeLookup : (int*int*tree) -> int list (*take two integers low and high, return a int list of all values between low and high*)

  val min : tree -> int (*return the min*)

  val max : tree -> int (*return the max*)

  val searchByRank : (int*tree) -> int option (*take a int k return the kth smallest vlaue in the tree*)

  val delete : (int*tree) -> tree (*FOR EXTRA CREDIT: delete int from tree, true if deleted false if no such node*)

end;


structure avl :> TREE =
struct

  datatype tree = Node of ( tree * int * tree ) | Null

  fun height ( Null ) = 0
  |   height ( Node ( lt, c, rt ) ) =
    let
      val lh = height ( lt )
      val rh = height ( rt )
    in
      if lh > rh then lh + 1
      else rh + 1
    end;

  fun bal_factor ( Null ) = 0
  |   bal_factor ( Node ( lt, x, rt ) ) =
    height( lt ) - height( rt );

  fun AVLbalance( Null ) = Null
  |   AVLbalance( Node( Null, x, Null ) ) = Node( Null, x, Null )
  |   AVLbalance( Node( lt, c, rt ) ) =
    let
      val brt = AVLbalance( rt )
      val blt = AVLbalance( lt )
      val bfact = bal_factor( Node( blt, c, brt ) )
    in
      case ( Node( blt, c, brt ), bfact ) of
        ( Node( Node( llt, cl, Node( lrlt, clr, lrrt ) ), c, brt ), 2 ) =>
          if height( llt ) > height( Node( lrlt, clr, lrrt ) ) (* if l tree greater,
  								then single rotate*)
  	then
  	  Node( llt, cl, Node( Node( lrlt, clr, lrrt ), c, rt ) ) (* Otherwise double rotate *)
  	else
  	  Node( Node( llt, cl, lrlt ), clr, Node( lrrt, c, brt ) )
      | ( Node( Node( llt, cl, lrt ), c, rt ), 2 ) => (* This is needed for that one case that is
  	                                               not caught by the first pattern match *)
  	Node( llt, cl, Node( lrt, c, rt ) )
      | ( Node( blt, c, Node( Node( rllt, crl, rlrt ), cr, rrt ) ), ~2 ) =>
          if height( rrt ) > height( Node( rllt, crl, rlrt ) )
  	then
  	  Node( Node( lt, c, Node( rllt, crl, rlrt ) ), cr, rrt )
  	else
  	  Node( Node( blt, c, rllt ), crl, Node( rlrt, cr, rrt ) )
      | ( Node( lt, c, Node( rlt, cr, rrt ) ), ~2 ) =>
  	Node( Node( lt, c, rlt ), cr, rrt )
      | ( balTree, n ) => balTree
    end;

  fun add ( x, Null ) = Node( Null, x, Null )
  |   add ( x, Node ( lt, c, rt ) ) =
    if x > c
    then
      AVLbalance( Node( lt, c, add ( x, rt ) ) )
    else
      AVLbalance( Node( add ( x, lt ), c, rt ) );

  fun create( l ) =
    let
      fun addlist( [], tree ) = tree
      |   addlist( l, tree ) = addlist( tl( l ), add( hd( l ), tree ) )
    in
      addlist( l, Null )
    end;

  fun min( tree ) =
    case tree of
      Node( Null, c, r ) => c
    | Node( l, c, r ) => min( l );


  fun max( tree ) =
    case tree of
      Node( l, c, Null ) => c
    | Node( l, c, r ) => max( r )

  fun lookup( x, Null ) = false
  |   lookup( x, Node( l, c, r ) ) =
    if x = c then true
    else if x > c then lookup( x, r )
    else lookup( x, l );

  fun rangeLookup( lo, hi, Null ) = []
  |   rangeLookup( lo, hi, Node( l, c, r ) ) =
    if lo <= c andalso c <= hi
    then
      rangeLookup( lo, hi, l ) @ [ c ] @ rangeLookup( lo, hi, r )
    else if lo > c
    then
      rangeLookup( lo, hi, r )
    else if hi < c
    then
      rangeLookup( lo, hi, l )
    else [];

  fun searchByRank( k, tree ) =
    let
      val data = rangeLookup( min( tree ), max( tree ), tree )
      fun findkth( 1, ( h :: t ) ) = SOME h
      |   findkth( n, [] ) = NONE
      |   findkth( n, ( h :: t ) ) = findkth( n - 1, t )
    in
      findkth( k, data )
    end;

  fun delete( x, Null) = Null
  |   delete( x, Node( lt, n, rt ) ) =
    if x = n
    then
      case Node( lt, n, rt ) of
        Node( Null, n, Null ) => Null
      | Node( lt, n, Null ) =>
          let
	    val mx = max( lt )
	  in
	    AVLbalance( Node( AVLbalance( delete( mx, lt ) ), mx, Null ) )
	  end
      | Node( lt, n, rt ) =>
          let
	    val mn = min( rt )
	  in
	    AVLbalance( Node( lt, mn, AVLbalance( delete( mn, rt ) ) ) )
	  end
    else if x > n then AVLbalance( Node( lt, n, delete( x, rt ) ) )
    else AVLbalance( Node( delete( x, lt ), n, rt ) );

end;
